# README #

p-secured-template-api


### What is this repository for? ###

* Process API for Okta Oauth2.0 Provider

### About Secured Template API###
The primary objective of this project is to provide a reusable template for the developers to connect to secured endpoint. 
The templated will request a an access_token and will attach to the headers before calling the secured endpoint. 

 
### Versions ###

## 1.0.0 ##
* Initial Commit

### How do I get set up? ###
* Maven
* Mule v3.9.0
* Java 1.8

### Contribution guidelines ###
* 

### Who do I talk to? ###
* Gards Ladines
